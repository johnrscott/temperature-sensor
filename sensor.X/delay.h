/**
 * \file delay.h
 * \brief Delay functions for the serial communication
 * 
 * The functions in this file are mainly written in assembly. Using a better
 * quality compiler (with optimisations enabled) would remove the need for this
 * low level of control
 * 
 */

#ifndef DELAY_H
#define	DELAY_H

#include <xc.h>

/// Cause a delay of 20us
void delay20us(void);

/// Cause a delay of 100us
void delay100us(void);

/// Cause a delay of 500us
void delay500us(void);

/// Cause a delay of 2ms
void delay2ms(void);

/// Cause a delay of 10ms
void delay10ms(void);

/// Cause a delay of 100ms
void delay100ms(void);

void setBusToIdle(void);

/**
 * \brief Briefly pulse the bus low
 * 
 * It is sometimes necessary to make a very short low pulse on the bus (for
 * example at the start of a write one time slot). This function causes a 
 * pulse that rises again with at most 15us
 * 
 */
void pulseBusLow(void);

#endif

