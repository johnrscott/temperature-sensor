   /**
    * \file asm.s
    * \brief Contains assembly functions for delaying and serial operations
    *
    * Some of the operations in serial communication protocol are time
    * time sensitive, requiring precise delays or low latency port operations. 
    * This file contains functions that are used elsewhere when timing is 
    * important
    *
    */
    
#include <xc.inc>

    GLOBAL _delay20us
    GLOBAL _delay100us
    GLOBAL _delay500us
    GLOBAL _delay2ms
    GLOBAL _delay10ms
    GLOBAL _delay100ms
    GLOBAL _setBusToIdle
    GLOBAL _pulseBusLow
    GLOBAL _enableStrongPullUp
    
    PSECT asm, class=CODE, delta=2

    // 4 cycles for call and return + 1 per nop = 10 cycles = 20us
_delay20us:
    nop
    nop
    nop
    return;
    
_delay60us:
    call _delay20us
    call _delay20us
    call _delay20us
    return;
    
_delay100us:
    call _delay20us
    call _delay20us
    call _delay20us
    call _delay20us
    call _delay20us
    call _delay20us
    return;
    
_delay500us:
    call _delay100us
    call _delay100us
    call _delay100us
    call _delay100us
    call _delay100us
    call _delay20us
    return

_delay2ms:
    call _delay500us
    call _delay500us
    call _delay500us
    call _delay500us
    call _delay100us
    return

_delay10ms:
    call _delay2ms
    call _delay2ms
    call _delay2ms
    call _delay2ms
    call _delay2ms
    return
    
_delay100ms:
    call _delay10ms
    call _delay10ms
    call _delay10ms
    call _delay10ms
    call _delay10ms
    call _delay10ms
    call _delay10ms
    call _delay10ms
    call _delay10ms
    call _delay10ms
    return
    
    
    // Cause a brief low signal on the bus (rises within 15us)
_pulseBusLow:
    BANKSEL(LATC)
    bcf BANKMASK(LATC), 2   // Prepare output state as low
    BANKSEL(TRISC)
    bcf BANKMASK(TRISC), 2  // Set port as output (set to low)
    BANKSEL(LATC)
    bsf BANKMASK(LATC), 2   // Pull output high before changing back to input
    BANKSEL(TRISC)
    bsf BANKMASK(TRISC), 2  // Set to input
    return

_setBusToIdle:
    BANKSEL(LATC)
    bsf BANKMASK(LATC), 2   // Pull bus high
    BANKSEL(TRISC)
    bsf BANKMASK(TRISC), 2  // Set bus to input (weak pull up state)
    return

_enableStrongPullUp:
    BANKSEL(LATC)
    bsf BANKMASK(LATC), 2   // Set output to high 
    BANKSEL(TRISC)
    bcf BANKMASK(TRISC), 2   // Set port to output (provides up to 50mA now)
    return
