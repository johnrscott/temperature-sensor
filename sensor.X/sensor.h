/**
 * \file sensor.h
 * \brief Function prototypes for communicating with the temperature sensor
 * 
 * The temperature sensor uses a one-wire serial interface for communication.
 * Each command issued from the microcontroller to the sensor involves three
 * steps: a reset, an addressing stage, and the command. For simplicity, when
 * there is only one temperature sensor, the addressing step can be skipped
 * (using the SKIP ROM command).
 * 
 * The only functions implemented here are write (for configuring the sensor),
 * convert T (for taking a temperature measurement), and read (for obtaining
 * the temperature measurement). Other commands and functionality are described
 * in the temperature sensor datasheet (ds18b20.pdf)
 */

#ifndef SENSOR_H
#define	SENSOR_H

#include <stdbool.h>
#include <stdint.h>

/**
 * \brief Initialise the one wire serial interface 
 * 
 * The serial interface for the sensor uses a one wire connection, using the 
 * following pin 
 * 
 * - TX/RX: RC0, LATC:0, pin 10 (output)
 * 
 * Both TX and RX are connected together and to the data line. When the strong
 * pull up state is needed (see datasheet), the line is set to an output to
 * source current.
 * 
 */
void initialiseSerial(void);

/**
 * \brief The structure of the temperature sensor scratchpad
 * 
 * This is the data returned by the temperature sensor when a read operation
 * is performed. It contains the temperature information and other configuration
 * details of the device
 * 
 */
typedef struct {
    uint8_t tempLSB; // Default 0x50
    uint8_t tempMSB; // Default 0x05
    uint8_t TH;
    uint8_t TL;
    uint8_t config;
    uint8_t reserved1; // Must be 0xff
    uint8_t reserved2;
    uint8_t reserved3; // Must be 0x10
    uint8_t crc;
} ScratchPad;

/**
 * \brief Perform a sensor reset
 * 
 * This function is used to reset the temperature sensor, ready to start
 * the next communication sequence. 
 * 
 * \return If the temperature sensor is present, the return value is true
 * 
 */
bool sensorReset(void);

/**
 * \brief Perform a ROM SKIP command
 * 
 * This function can be called after reset to address the only sensor on the 
 * bus. Do not use if there are multiple sensors (see the datasheet)
 */
void sensorROMCommandSkip(void);

/**
 * \brief Read the internal scratchpad of the sensor
 * 
 * This function can be used after reset and skip to read the internal
 * scratchpad of the sensor, which includes the temperature sensor data.
 * 
 * \return The return value contains the data returned by the sensor
 */
ScratchPad sensorFunctionReadScratchpad(void);

/**
 * \brief Write to the internal scratchpad of the sensor
 * 
 * This function can be used to write the config value to the internal
 * memory of the sensor. In particular, the config value can be used to
 * set the precision of the temperature sensor. Call the function after a
 * reset and skip command. See the datasheet for more information.
 * 
 * \param TH The value to write to T_H in the scratchpad
 * \param TL The value to write to T_L in the scratchpad
 * \param config The value to write to the config word in the scratchpad
 */
void sensorFunctionWriteScratchpad(uint8_t TH, uint8_t TL, uint8_t config);


/**
 * \brief Take a temperature measurement
 * 
 * Use this function to initiate a temperature measurement, after a reset
 * and skip command. This function must only be used when 9-bits precision
 * has been set using sensorFunctionWriteScratchpad(uint8_t, uint8_t, uint8_t)
 * because only 100ms of power is provided for the conversion (enough time
 * for the 9-bit process, but not for 12-bits).
 * 
 */
void sensorFunctionConvertT(void);

#endif

