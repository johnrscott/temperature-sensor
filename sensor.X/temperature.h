/**
 * \file temperature.h
 * \brief Contains the function for measuring the temperature
 *
 * The DS18B20 temperature sensor (datasheet ds18b20.pdf) is a 
 * one-wire digital thermometer that is accurate to 0.5 degrees C up to 85
 * degrees.
 * 
 * To use the function in this file, attach the data line of the temperature
 * sensor to RC0 and make sure initialiseSerial is called to set up the port
 * properly.
 * 
 */

#ifndef TEMPERATURE_H
#define	TEMPERATURE_H

#include <stdint.h>

/**
 * \brief Measure the temperture
 * 
 * This function performs a temperature measurement using the attached
 * DS18B20 device. The temperature is returned in the variable temp. The
 * return value is true if the temperature conversion was performed 
 * successfully, and false if there is a fault.
 * 
 * The temperature T in degrees C is encoded in the temp variable as 2T 
 * because the precision is half a degree. Divide the result by two (or better,
 * compare with scaled temperature values) to extract the true temperature. 
 * 
 * The function takes about 100ms to execute, most of which is the temperature
 * conversion.
 * 
 * \param temp After exiting, temp will contain the temperature reading 2T
 * \return The function returns false if the temperature sensor is faulty
 * 
 */
bool measureTemp(uint8_t * temp);

#endif

