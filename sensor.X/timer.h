/**
 * \file timer.h
 * \brief Set up the timers and interrupts
 * 
 * Call this function at the start of the program to set up the one second
 * interrupt routine isr(void) that controls the temperature sensor and handles
 * user input/output.
 * 
 */

#ifndef TIMER_H
#define	TIMER_H

/**
 * \brief Initialise the timing functions
 * 
 * This function sets up timer 1 to perform timing operations for the
 * temperature sensor. It enables an interrupt that is called every second.
 * The interrupt service routine isr(void) handles the temperature measurements
 * and input/output control.
 */
void initialiseTimer(void);

#endif
