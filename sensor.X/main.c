#include <xc.h>
#include <stdbool.h>
#include "io.h"
#include "timer.h"
#include "sensor.h"
#include "temperature.h"

#pragma config WDTE = OFF
#pragma config FOSC = INTOSC // Use the internal oscillator

int main(void) 
{
    // Set up the microcontroller to use a clock rate of 2MHz, 
    // so instruction clock is 500kHz (2us period).
    OSCCONbits.IRCF = 0xc; // 0b1100
   
    // Set up the four LEDs and the reset button
    initialiseIO();
    
    // Set up the serial communication with the microcontroller
    initialiseSerial();
    
    // Initialise the timing module which produces triggers an interrupt 
    // every second. See timer.c for the interrupt service routine (isr)
    // which contains most of the content of the program
    initialiseTimer();
    
    // The only thing that happens in main is the reset button is repeatedly
    // polled. If the button has been pressed, this event is stored and later
    // read by the interrupt service routine
    while(true) {
        waitForButton(); // Stores the button state
    }

}
