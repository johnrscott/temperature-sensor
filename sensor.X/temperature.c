/**
 * \file temperature.c
 * \brief Implementation of the temperature measurement
 * 
 * The main content of the measureTemp function is a system for keeping track
 * of the state of the temeprature sensor. The temperature sensor can be 
 * connected and disconnected at any time, and repeated calls to measureTemp
 * will configure the sensor if necessary.
 * 
 */

#include "sensor.h"
#include "io.h"
#include <stdint.h>


typedef enum {
    DISCONNECTED, CONNECTED, CONFIGURED, READOUT
} State;

/// An internal global variable to record the sensor state
static State state = DISCONNECTED;

/**
 * \brief Print the current state for debugging
 * 
 * This function can be used to print the current state of the
 * temperature sensor on the LEDs. 
 * 
 * \param state Contains the state to be printed
 */
static void printState(State state) {

    switch(state) {
        case DISCONNECTED:
            displayNumber(1);
            break;
        case CONNECTED:
            displayNumber(2);
            break;
        case CONFIGURED:
            displayNumber(4);
            break;
        case READOUT:
            displayNumber(8);
            break;
    }
    
}

/**
 * \brief Take a temperature measurement
 * 
 * This function tests whether the sensor is connected. If so, it configures
 * it, and then takes a temperature reading. If at any point a sensorReset fails
 * (returns false), this function also returns false indicating a problem with
 * the temperature sensor.
 * 
 * \param temp A pointer to an integer in which to store the temperature reading
 * \return The return value is false if the temperature sensor is not connected
 *  
 */
bool measureTemp(uint8_t * temp) {
    
    ScratchPad scratch; // A variable to hold the internal sensor scratchpad
    
    switch(state) {
        case DISCONNECTED:
            /*
             * Perform a sensor reset to check if a sensor is connected. If
             * not, return false. If there is a connected sensor, progress 
             * to the next state (configuration)
             */
            printState(state);
            if(sensorReset() == true) {
                state = CONNECTED;
            } else {
                return false; // No sensor present
            }
            // Fall through
        case CONNECTED:
            /*
             * Now that a sensor is connected, set the sensor resolution to
             * 9-bits by writing 0x1f to the configuration word. Then progress
             * to the temperature measurement. If the temperature sensor has
             * been disconnected, return false and set state to disconnected.
             */
            printState(state);
            if(!sensorReset()) {
                state = DISCONNECTED;
                return false; // No sensor present
            } else {
                sensorROMCommandSkip();
                sensorFunctionWriteScratchpad(scratch.TH, scratch.TL, 0x1f);
                state = CONFIGURED;
            }
            // Fall through
        case CONFIGURED:
            /*
             * Take a temperature measurement. As usual, return false if the
             * sensor has been disconnected. This part of the routine takes
             * the longest due to the delay involved in converting the 
             * temperature.
             */
            printState(state);
            if(!sensorReset()) {
                state = DISCONNECTED;
                return false; // No sensor present
            } else {               
                sensorROMCommandSkip();
                sensorFunctionConvertT();
                state = READOUT;
            }
            // Fall through
        case READOUT:
            /*
             * Recall the temperature from the sensor. If the sensor is not
             * connected anymore, then return false. The data is returned from
             * the sensor in the scratch struct. The low and high words of 
             * the sensor are shifted appropriately and combined to introduce
             * a factor or two in the result (to ensure that the returned
             * value is an integer). See the datasheet for more information.
             * T
             */
            printState(state);
            if(!sensorReset()) {
                state = DISCONNECTED;
                return false; // No sensor present
            } else {
                sensorROMCommandSkip();
                scratch = sensorFunctionReadScratchpad();
                uint8_t high = (scratch.tempMSB & 0x7) << 5;
                uint8_t low = scratch.tempLSB >> 3;       
                * temp = (high | low); // Write the temperature to temp pointer
                state = CONFIGURED;
                return true; // Return true to indicate valid reading
            }
    }
    
}
