/**
 * \file sensor.c
 * \brief Implementation of the sensor one-wire serial communication
 */

#include <xc.h>
#include <stdbool.h>
#include "io.h"
#include "timer.h"
#include <stdbool.h>
#include "sensor.h"
#include "delay.h"  

/**
 * \brief Initialise the one wire serial interface 
 * 
 * The serial interface for the sensor uses a one wire connection which 
 * is used for communicating with the temperature sensor. Three pins are
 * used for the bus communication
 * 
 * - TX: RC0, LATC:0, pin 10 (output)
 * - PEN (pull up): RC1, LATC:1, pin 9 (output)
 * 
 * Both TX and RX are connected together and to the data line. The pull-up
 * enable PEN controls a pull-up transistor
 * 
 */
void initialiseSerial(void) {
 
    // Set up the pull-up enable output
    ANSELCbits.ANSC1 = 0; // Digital pin
    TRISCbits.TRISC1 = 0; // PEN
    LATCbits.LATC1 = 1; // Set to default high
    
    // Globally enable individual weak pull-ups
    OPTION_REGbits.nWPUEN = 0;
    
    // Set up TX/RX for input
    ANSELCbits.ANSC2 = 0; // Digital pin
    TRISCbits.TRISC2 = 1; // Input
    WPUCbits.WPUC2 = 1; // Set as weak pull-up
    
    // Set TX/RX for output
    LATCbits.LATC2 = 0; // Set output to low
    
    // To pull the bus low, set the TRIS bit to 0. Set to 1 to 
    // reset to normally high level
    
    return;
}

/**
 * \brief Pull the one-wire data line low
 * 
 * In the data communication between the microcontroller and the sensor, the 
 * single data line is normally high (5V), due to the weak pull-up. Use this
 * function to pull the line low (to 0V).
 * 
 */
void pullBusLow(void) {
    LATCbits.LATC2 = 0; // Set output state to low
    TRISCbits.TRISC2 = 0; // Set bus to output state
    return;
}

/**
 * \brief Reset the bus
 * 
 * Use this function at the start of any communication sequence with the
 * temperature sensor. The reset pulse consists of a active low pulse from
 * the microcontroller which lasts for at least 460us, followed by a presence 
 * pulse from the sensor. If there is no presence pulse, it is assumed that
 * the sensor is not present and the function returns false.
 * 
 * \return The function returns true if the sensor is present
 */
bool sensorReset(void) {
    
    delay100us();
    
    // Send a reset pulse
    pullBusLow();
    delay500us();
    setBusToIdle();

    // Wait for presence pulse
    size_t count = 0;
    bool present = false;
    while(count < 10) {
        if(PORTCbits.RC2 == 0) {
            present = true;
            break;
        }
        count ++;
        delay20us();
    }
    
    while(PORTCbits.RC2 == 0)
       ; // Wait for return to idle
    
    // Delay for 500us
    delay500us();

    return present;
}

/**
 * \brief Write a single bit to the sensor
 * 
 * This function executes a `write time slot` which writes a single bit of
 * data to the temperature sensor. It is used as a subroutine in all the
 * other command functions. The bit of data is stored in a uint8_t type, 
 * which should be either zero (for 0) or non-zero (for 1). After the
 * write has finished, bus power can be enabled, which is used for the special
 * case where a temperature conversion follows the write. 
 * 
 * \param d Write a zero if d == 0, otherwise write one
 * \param power If true, then bus power is enabled after the write
 */
void writeTimeSlot(uint8_t d, bool power) {
    
    if(d == 0) {
        pullBusLow();
    } else {
        pulseBusLow();
    }
    
    // Delay for duration of time slot
    delay100us();

    // Reset bus to idle state    
    if(power == false) {
        setBusToIdle();
    } else {
        // Enable strong pull up
        //LATCbits.LATC1 = 0;
        //setBusToIdle();
        LATCbits.LATC2 = 1; // Set output high
        TRISCbits.TRISC2 = 0; // Enable output
    }
            
    return;
    
}

/**
 * \brief Read a single bit from the sensor
 * 
 * Use this function to read a single bit of data from the device. The function
 * is used as a subroutine in the commands that request data from the device
 * 
 * \return The function returns the bit. Interpret zero a 0 and non-zero as 1.
 */
uint8_t readTimeSlot(void) {
  
    pulseBusLow(); // Start by pulsing the bus low
    uint8_t value = PORTCbits.RC2; // You don't need a delay before this line
    delay100us(); // Delay for rest of time slot
    return value;
    
}

/**
 * \brief Read a byte from the sensor
 * \return The function returns the byte that was read
 */
uint8_t readByte(void) {
    
    uint8_t data = 0;
    
    // Read each bit (1us recovery between bits built in)
    for(uint8_t k=0; k<8; k++) {
        data |= (readTimeSlot() << k);
    }

    return data;
    
} 

/**
 * \brief Write a byte of data to the device
 * \param data The data to be written to the device
 */
void writeByte(uint8_t data) {
    
    // Write each bit (1us recovery between bits built in)
    for(uint8_t k=0; k<8; k++) {
        writeTimeSlot((data >> k) & 1, false);
    }
}

/**
 * \brief Write a number of bits to the device
 * 
 * This function is helper function used by the temperature conversion routine
 * to apply power after the last bit has been written
 * 
 * \param data Contains up to eight bits of data
 * \param n The number of least significant bits of \param data to write
 * \param power Whether to apply power after the write
 */
void writeBits(uint8_t data, int n, bool power) {
    
    // Write each bit (1us recovery between bits built in)
    for(uint8_t k=0; k<n; k++) {
        writeTimeSlot((data >> k) & 1, power);
    }
}

/**
 * \brief Send a SKIP command to the sensor
 * 
 * This function is used to avoid the addressing commands when there is only 
 * one temperature sensor connected to the bus. It is used after the reset
 * pulse has been issued
 * 
 * This command is issued by writing 0xcc to the bus.
 * 
 */
void sensorROMCommandSkip(void) {
    writeByte(0xcc);
    return;
}

/**
 * \brief Read the internal scratchpad of the sensor
 * 
 * This function writes 0xbe to the sensor, and then reads 9 successive bytes
 * from the device. The main point of this function is to get temperature
 * measurements from the device. See the datasheet for more information.
 * 
 * \return The scratchpad data is returned as a struct
 */
ScratchPad sensorFunctionReadScratchpad(void) {
    
    ScratchPad scratch;
    
    // Send function byte
    writeByte(0xbe);
    
    // Read data
    scratch.tempLSB = readByte(); // Default 0x50
    scratch.tempMSB = readByte(); // Default 0x05
    scratch.TH = readByte();
    scratch.TL = readByte();
    scratch.config = readByte();
    scratch.reserved1 = readByte(); // Must be 0xff
    scratch.reserved2 = readByte();
    scratch.reserved3 = readByte(); // Must be 0x10
    scratch.crc = readByte();
    
    ///\todo Implement the cyclic redundancy check 

    return scratch;
   
}

/**
 * \brief Write to the internal scratchpad
 * 
 * The sensor has three fields which can be written by the user: T_H,
 * T_L and config. Of these, the config field is the most important because
 * it contains the temperature sensor resolution. The other fields set
 * temperature alarm limits which are useful if several devices are connected
 * to the bus (see the datasheet).
 * 
 * The config field contains two bits (bit 5 and 6) which specify the 
 * sensor resolution. In turn, the temperature sensor resolution sets the
 * length of time required for the temperature conversion, as given by the
 * following table
 * 
 * | bit 6 | bit 5 | Resolution | Conversion time/ms |
 * |-------|-------|------------|--------------------|
 * |     0 |     0 | 9-bit      | 93.75              |
 * |     0 |     1 | 10-bit     | 187.5              |
 * |     1 |     0 | 11-bit     | 375                |
 * |     1 |     1 | 12-bit     | 750                |
 * 
 * \param TH Value to write to the T_H field
 * \param TL Value to write to the T_L field 
 * \param config Value to write to the config field
 */
void sensorFunctionWriteScratchpad(uint8_t TH, uint8_t TL, uint8_t config) {
    
    // Write the function byte (0x4e for a write)
    writeByte(0x4e); 
    
    // Write the data bytes
    writeByte(TH); 
    writeByte(TL);
    writeByte(config);
    return;   
}

/**
 * \brief Take a temperature measurement
 * 
 * Use this function to initiate a temperature sensor measurement. The result
 * if the reading is stored in the internal scratchpad, and must be read
 * later using the sensorFunctionReadScratchpad(void) function. This function
 * blocks execution for approximately 100ms while the conversion takes place.
 * 
 * This function makes use of the writeBits(uint8_t, int. bool) function to
 * specifically enable bus power at the end of the write command. This is
 * necessary because of the strict timing requirement that bus power is enabled
 * within 10us of the end of the convert T command (see the datasheet).
 * 
 * It is important to set the sensor resultion to 9-bit to use this function,
 * because otherwise the temperature reading may be inaccurate.
 * 
 */
void sensorFunctionConvertT() {
    
    // Write 0x44 manually
    writeBits(0x44, 7, false); // Write first 7 least significant bits
    writeBits(0x0, 1, true); // Write last zero, and enable bus power

    // Delay during temperature conversion (at least 100ms)
    delay100ms();
   
    // Disable strong pull up
    LATCbits.LATC1 = 1;
    setBusToIdle();
    
    return;   
}
