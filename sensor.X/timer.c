/**
 * \file timer.c
 * \brief Contains the main timing program
 * 
 * This file contains the interrupt service routine isr(void) which performs
 * the main timing function of the program. The routine is called once every
 * second and checks the state of the button and the temperature sensor
 * 
 */
#include <stddef.h>
#include <stdbool.h>
#include "io.h"
#include "temperature.h"
#include "delay.h"
#include <xc.h>

#define OVER_TEMP 90 // Degrees
#define UNDER_TEMP 70 // Degrees
#define DURATION 1800 // Seconds (30mins)

void initialiseTimer(void) {

    // Enable interrupts globally
    INTCONbits.GIE = 1;
    INTCONbits.PEIE = 1; // Enable peripheral interrupts

    // Set pre-scale to 1:8
    T1CONbits.T1CKPS = 0x3;

    // Enable timer 1 (16-bit) interrupt
    PIE1bits.TMR1IE = 1;
    TMR1H = 0x0b; // Set counter to time 1 second
    TMR1L = 0xdc;

    // Enable timers
    T1CONbits.TMR1ON = 1;
}

/// Counter to time 30mins as 1800 seconds
static int count = 0;

/**
 * \brief Store the current state of the sensor 
 * 
 * This variable stores the current state of the temperature sensor. The
 * READY state indicates that the system is waiting for the start button to
 * be pressed. Once it is pressed, the system starts measuring the temperature
 * and timing. There are three possible states during this phase: NORMAL_TEMP,
 * OVER_TEMP and UNDER_TEMP, which control how the timing proceeds. If 30mins
 * elapses in either the NORMAL_TEMP or OVER_TEMP state, then the system
 * reverts back to the READY state. If there is a fault with the sensor at any
 * point, the system enters the FAULT state, which is cleared by pressing the
 * start button.
 *
 */
static struct {
    bool fault; 
    bool over_temp;
    bool under_temp;
    bool ready;
    int timer; // cooking time from 0 to 1800 seconds
} state = { false, false, false, true, 0};

/**
 * \brief Print the current state
 * 
 * This function prints the current state onto the LEDs as follows. The FAULT
 * and OVER_TEMP, which are fault conditions, map to their own LEDs. The READY
 * state corresponds to its own LED (green). The final two states, NORMAL_TEMP
 * and UNDER_TEMP, both correspond to an `in progress` LED.
 * 
 * This function flashes the LEDs on for 100ms before switching them off. The
 * function is designed to be called regularly at a rate of 1 second.
 * 
 */
static void printState(void) {

    if(state.fault == true) {
        setD7(true); // Fault LED
    } else {

        // Test for over temperature
        if(state.over_temp == true) {
            setD6(true); // Over temperature LED
        }
        else if(state.under_temp == true) {
            // whatever the under temp action is.
        }

        // Distinguish between READY and IN-PROGRESS
        if(state.ready == true) {
            setD4(true); // Ready LED
        } 
        else {
            setD5(true); // `In progress` LED
        }  
    }
    delay100ms();
    clearLEDs();
}

#define INTERRUPT __interrupt() ///< You need this so that doxygen works

/**
 * \brief Interrupt service routine for timer 
 * 
 * This function contains the main content of the program. The function is
 * called once every second by the timer module. The routine checks whether
 * the reset button has been pressed. If it has, it starts the counter which
 * increments every second, provided that the temperature is hot enough. Once
 * the timer reaches 30mins, the ready light is turned back on.
 * 
 * If at any time the fault condition is raised, the fault light is turned on.
 * A power cycle is required to clear the fault state.
 * 
 */
void INTERRUPT isr(void)
{   
    // First, reset the timer counters  to time the next second
    TMR1H = 0x0b;
    TMR1L = 0xdc;

    // Clear the interrupt flag bit
    PIR1bits.TMR1IF = 0;

    // wait for button to start the timer
    if(state.ready == true) {
        
        // if button pressed, start the oven and set ready to (false)
        if(getButtonState() == true) {
            state.ready = false;
        }
        
    }

    // This variable records the temperature
    uint8_t temp = 0;

    // If measureTemp returns true there is not a fault 
    // Do not take measurement in fault state
    if(measureTemp(&temp) && state.fault == false)
    {
        // Check if temperature meets lower limit
        if(temp < 2 * UNDER_TEMP)  {
            state.under_temp = true; // Record under temperature condition
        } else {
            state.under_temp = false;
        }
        
        if(temp > 2 * OVER_TEMP) {
            state.over_temp = true; // Record over temperature condition
        } else {
            state.over_temp = false;
        }
        
    } else {
        state.fault = true; // Record temperature fault
    }

    // If there is not fault and the temperature is above the lower limit,
    // increment the counter
    if((state.fault == false) && (state.under_temp == false) 
            && (state.ready == false))
    {
        state.timer++; // Increment counter 
        
        // Now check for 30min timeout
        if(state.timer >= DURATION) {
            // reset timer and turn on ready light 
            state.timer = 0;
            state.ready = true;
        }
    }
    
    // Flash LEDs once every 3 seconds
    static int printCount = 0;
    if(printCount == 2) {
        printState();
        printCount = 0;
    } else {
        printCount++;
    }
    return;
}
