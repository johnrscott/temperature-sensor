/**
 * \file io.h
 * \brief Contains functions for writing to LEDs and reading buttons
 */

#ifndef IO_TOOLS_H
#define	IO_TOOLS_H

#include <stdbool.h>
#include <stdint.h>

/**
 * \brief Set up the LEDS and buttons
 * 
 * This function sets up the I/O ports so that the four LEDs are outputs and
 * the button is connected to an input. 
 * 
 */
void initialiseIO(void);

/// Set LED D4 to on/off
void setD4(bool state);

/// Set LED D4 to on/off
void setD5(bool state);

/// Set LED D4 to on/off
void setD6(bool state);

/// Set LED D4 to on/off
void setD7(bool state);

/// Switch LED D4 to on if it's initially off and vice-versa
void toggleD4(void);

/// Switch LED D5 to on if it's initially off and vice-versa
void toggleD5(void);

/// Get current state of button (pushed down or up)
bool isButtonPressed(void);

/// Returns once the user has pressed a button
void waitForButton(void);

/// Check whether a button has been pressed
bool getButtonState(void);

/// Display an integer on the LEDs in binary
void displayNumber(uint8_t n);

/// Clear all the LEDs
void clearLEDs(void);

#endif
