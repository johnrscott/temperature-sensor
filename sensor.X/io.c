/**
 * \file io.c
 * \brief Contains input/output functions for the LEDs and buttons
 * 
 * Call initialiseIO(void) at the start of the main program to use the LEDs and
 * buttons. 
 */

#include "io.h"
#include <xc.h>

/**
 * \brief Set up the LEDS and buttons
 * 
 * This function sets up the I/O ports so that the four LEDs are outputs and
 * the button is connected to an input. 
 * 
 */
void initialiseIO(void) {
    
    // Set up LEDs - configure pins as outputs
    TRISAbits.TRISA5 = 0;   // D4
    TRISAbits.TRISA1 = 0;   // D5
    TRISAbits.TRISA2 = 0;   // D6
    TRISCbits.TRISC5 = 0;   // D7
    
    // Initialise all the LEDs to off
    LATA = 0;
    LATC = 0;
    
    // Set up button S1
    TRISCbits.TRISC4 = 1;  // Configure pin as input
    ANSELCbits.ANSC4 = 0;  // Set to digital input

}


/*********************************************************************/
void setD4(bool state)
{
    if (state) {
        // Turn on the LED
        LATAbits.LATA5 = 1;
    }
    else {
        // Turn off the LED
        LATAbits.LATA5 = 0;
    }
}


/*********************************************************************/
void setD5(bool state)
{
    if (state) {
        // Turn on the LED
        LATAbits.LATA1 = 1;
    }
    else {
        // Turn off the LED
        LATAbits.LATA1 = 0;
    }
}


/*********************************************************************/
void setD6(bool state)
{
    if (state) {
        // Turn on the LED
        LATAbits.LATA2 = 1;
    }
    else {
        // Turn off the LED
        LATAbits.LATA2 = 0;
    }
}


/*********************************************************************/
void setD7(bool state)
{
    if (state) {
        // Turn on the LED
        LATCbits.LATC5 = 1;
    }
    else {
        // Turn off the LED
        LATCbits.LATC5 = 0;
    }
}


/*********************************************************************/
bool isButtonPressed(void)
{
    // Returns true if the button is pressed
    if (PORTCbits.RC4 == 0) {
        return true;
    }
    else {
        return false;
    }
}

static bool button_state = false;

bool getButtonState(void) {
    if(button_state == true) {
        button_state = false; // Reset state
        return true;
    } else {
        return false;
    }
}

/*********************************************************************/
void waitForButton(void)
{
    // Wait for previous button to be released
    while (isButtonPressed() == true);
    // Wait for current button press
    while (isButtonPressed() == false);
    
    // Store the button press
    button_state = true;
    
    return;
}


/*********************************************************************/
void displayNumber(uint8_t n) 
{
    // Return if the number is bigger than 15
    // TO DO: add some kind of error alert e.g. flashing lights
    if (n > 16) 
        return;
    
    clearLEDs();
    // Turn on the relevant lights
    // D4 is the least significant bit    
    if ((n & 1) == 1) {
        setD4(true);
    }
    if ((n & 2) == 2) {
        setD5(true);
    }
    if ((n & 4) == 4) {
        setD6(true);
    }
    if ((n & 8) == 8) {
        setD7(true);
    }
}


/*********************************************************************/
void clearLEDs(void)
{
    setD4(false);
    setD5(false);
    setD6(false);
    setD7(false);
}
