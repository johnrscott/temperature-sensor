# Specification

This device accurately monitors temperature whilst timing for a user defined amount of time. We are using it to monitor heat tterilisation of Covid-19 PPE. Current research shows that heating items over 70C (degrees) for at least 30 minutes significantly reduces the concentration of virus present.

## Behaviour

The device is composed of multiple parts
![](/pcb/sensor/sensor.png)

1. A controller board, with on/off switch that is battery powered or solar panel (5v)
2. Four LEDs: `In progress` (red), `Ready` (green), `Over temperature` (red), `Fault` (red).
3. A reset button, large enough to be elbow operated
4. A remote temperature sensor

The system works as follows:

1. The on state for LEDs is a periodic flashing pattern, in order to save power compared to a steady on state.
2. When powered on, a self check is performed to ensure that the temperature sensor is connected. If the temperature sensor does not respond or appears damaged in any way, the `fault` LED turns on. This indicates that the temperature sensor should be replaced. Once the sensor has been replaced the device must be restarted before use.
3. If the test passes, the `ready` LED is turned on.
4. When the reset button is pressed, the `ready` LED is switched off and then `in progress` LED is turned on. The temperature at the remote location is read at period intervals. After the temperature has exceeded 70 deg for at least 30mins, the `in progress` LED is turned off and the `ready` LED is turned back on, indicating that the process has finished.
5. If at any point the temperature rises above 90 deg, the `over temperature` LED is lit.
6. At any time, a fault condition in the temperature sensor will cause all LEDs to turn off apart from the `fault` LED. A power cycle is required to clear the fault LED

# Design

1. The temperature sensor used by the design in the DS18B20-PAR digital thermometer, which measures temperatures up to 100 deg, with error within 0.5% up to 85 deg. The device is read digitally using a 1-wire serial interface, which also supplies power. The digital transmission of temperature means that it is not necessary to correct for effects due to a long cable between the sensor and the controller board. The device requires no external components and derives power from the serial connection, meaning only two wires are necessary to connect the sensor to the board. This makes it simple to assemble the device and replace the sensor if it fails. The device has low power consumption (see below).

2. The user interface is made from a push button and LEDs, interfaced directly the microcontroller using current limiting resistors.

3. The main control is implemented using an 8-bit Microchip PIC16f1703 microcontroller. The LEDs and reset button are connected directly to the controller ports. The microcontroller implements the serial interface required by the temperature sensor as outlined in the DS18B20-PAR datasheet. A short summary is as follows:

## Temperature sensor serial link

Each communication with the sensor involves performing the following three steps: 
1. **Initialisation** This consists of a *reset* pulse transmitted by the microcontroller following by a *presence* pulse issued by the sensor. The reset pulse is a minimum 480us low signal from the microcontroller, followed by switch to recieving mode (a weak pull up using a 5k resistor). The sensor then waits 15-60us and then pulls the line low for 60-240us. This completes the initialisation.
2. **ROM command** A ROM command is issued by the microcontroller to identify and address sensors on the bus. There are 5 ROM commands, each encoded using a byte:
  - **SEARCH (0xf0)** Used to identify devices on the bus. Not necessary if there is only one device
  - **READ (0x33)** Used to read the 64-bit ROM code of the single device on the bus, which acts like its address. Only to be used with one device. If there are multiple devices, use *SEARCH* instead.
  - **MATCH (0x55)** Used to address a specific device on the bus, identified through it's 64-bit ROM code.
  - **SKIP (0xcc)** Used to address all devices simultaneously (or address the single device on the bus) without using ROM codes.
  - **ALARM SEARCH (0xec)** Like search, but devices will only respond if they have an alarm flag set.
  
  The diagram on page 11 of the datasheet shows the structure of the ROM commands. For a one-device bus, it is possible to issue a skip command in order to address the single device on the bus without using the 64-bit ROM code.

3. **Function command** A function command is used to request an operation by the addressed device. The functions are encoded using a single byte as follows
   - **Convert T (0x44)** Used to request a temperature conversion. The microcontroller must then enable the strong (MOSFET) pull up to provide power to the sensor within 10us of the command. At 9-bit temperature resolution (perfectly acceptable for the +- 0.5 deg precision), the strong pull up must last for at least 93.75ms. The temperature is stored in the scratch pad memory.
   - **Write scratchpad (0x4e)** This command is used to write 3 bytes to the scratch pad memory. Bytes are sent least significant bit first, and write to the T\_H, T\_L and configuration register (in that order). All three bytes must be written.
   - **Read scratchpad (0xbe)** This command is used to read bytes to the scratch pad memory. Bytes are read starting from the least significant bit of byte zero and ending with the 9th byte. Bytes zero and one contain the temperature data (LSB followed by MSB). The master may send a *reset* pulse at any time to terminate the read if the required data has been obtained.
   
   The microcontroller must issue read time slots (see below) after sending the read command so that the sensor can transmit data on the bus.
   
Data is read and written on the bus using *read time slots* and *write time slots*, used to transmit or recieve one bit of information at a time. Both write and read time slots are initiated by the microcontroller. They have the following structure. All time slots last between 60us and 120us, and must be separated by at least a 1us recovery time.
   - **Write "0" time slot** To write a logical zero, the microcontroller pulls the bus low for the duration of the time slot (60 - 120us), before releasing it for the recovery time.
   - **Write "1" time slot** To write a logical one, the microcontroller pulls the bus low for at least 1us, before releasing it within 15us for the rest of the time slot (up to 120us).
   - **Read time slot** A read time slot allows the sensor to transmit a logical one or zero to the micocontroller. The microcontroller pulls the bus low for at least 1us and then releases it within 15us for the rest of the time slot, similar to a write "1". However, in the 45us that follow, the sensor will either pull the bus low (indicating a "0") or leave it high (indicating a "1").

The microcontroller must execute the following sequence of operations to control the sensor

1. On power on, send a *reset* pulse and await the *presence*. The absense of the *presence* pulse is an error that indicates a faulty sensor (see below)
2. Set up the sensor to use 9-bit mode instead of the default 12-bits
   - Send reset/receive presence
   - Send the *skip* ROM command to address the single sensor on the bus
   - Use the *scratchpad write* command to set the temperature sensor resolution to 9 bits. The resolution is set by writing 0x1f to the configuration register.
   - Send reset/receive presence
   - Send the *skip* ROM command to address the single sensor on the bus
   - Send a *convert T* command
   - Send reset/receive presence
   - Send the *skip* ROM command to address the single sensor on the bus
   - Send a *read scratchpad* command to read the temperature data

## Microcontroller setup

The microcontroller is operated at a clock rate of 2MHz (which gives an instruction clock rate of 500kHz). This provides a fundamental operation time on the order of 2us, which is sufficient to achieve the timing specification of the sensor. The lower the clock rate the better, due to the higher power consumption of higher clock rates. The design has been prototyped on a Curiosity Low Pin Count board, which contains 4 LEDs and one push button (see photo). The connections are as follows:

  - LED D4: pin 2, RA5, LATA bit 5
  - LED D5: pin 12, RA1, LATA bit 1
  - LED D6: pin 11, RA2, LATA bit 2
  - LED D7: pin 5, RC5, LATC bit 5
  - Push button S1: pin 6, RC4, LATC bit 4
  
In addition, the temperature sensor requires a one-wire serial connection, which will be connected to RC0, RC1, RC2. The pins function as follows:

  - TX/RX (microcontroller output): pin 10, RC0, LATC bit 0
  
The TX/RX pin is connected directly to the data line. The TX/RX port is configured as an open-drain with weak pull-up input or a standard digital output depending on the function required (read or write). While operating as an input, the weak pull-ups supply a current up to 200uA (see datasheet), which is enough to supply the 5uA needed by the sensor during most activities. The 1.5mA required during a temperature is provided by switching the port to output high
 
# Power consumption

The device is designed to operate from 3x AAA size batteries in series, which provides about 4.5V each in total. (The device has been tested on a 5V supply but 4.5V lies comfortably inside the operating range of both the microcontroller and the sensor.) An estimate of the total capacity available from this power source is 1000mAh (the same as a single AAA battery). 

The current consumption of the device is made of three parts:
	1. the baseline current consumption of the microcontroller. This is specified by the datasheet as less than 1.6mA (see table 26-2 on page 270, param no. D019).
	2. the current consumption of the sensor. This is 1.5mA for approximately 100ms every second, which averages to 150uA. The baseline current consumption of the sensor is negligable (5uA).
   3. The current consumption of the blinking LEDs. These consume 20mA each for 100ms every 3 seconds. The worst case scenario occurs with two LEDs on at a time (`in-progress` and `too hot`). This will draw 0.67mA on average.

Hence the total current consumption in the worst case scenario (all LEDs on) is 2.32mA. The batteries will therefore last for 431 hours (approximately two and a half weeks if used continuously, or about one month if switched off at night).

## Solar power supply

Due to the low current consumption, it should be possible to power the sensor from a solar panel. Included on the board is a solar power connector with a switch to enable or disable solar vs. battery power. Solar power should be field tested for viability. Further improvements to the system included a system for solar power charging of the batteries. This would increase the stability of the power source, which may improve reliability, at the expense of extra complexity of the system.

# Error detection

The thermometer does not explicitly report failure states. However, if the sensor breaks in such a way that it fails to communicate correctly, the microcontroller can detect this condition and signal a sensor fault. It is not possible to detect the case where the device fails to produce accurate readings but otherwise appears to work. It may be possible to read from two identical sensors to build some assurance into the system.

## Improvements

The vast majority of the power consumption is from the LEDs. It would be better to replace these with a low power alternative (maybe a servo actuated dial or similar) so that the low power nature of the microcontroller is properly leveraged.

Several temperature sensors could easily be incorporated with no modification to the hardware. The software would need to be redesigned to include the more complex communication protocol. Multiple sensors would allow redundancy checking which would improve the reliability of the system.

Use of an external clock instead of the internal oscillator of the PIC16F would allow use of the low power mode of the device, which would reduce to current consumption to 420uA for no reduction in clock rate. This would further improve the battery life of the device if combined with a low power state indication mechanism.
