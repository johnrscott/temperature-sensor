var searchData=
[
  ['sensorfunctionconvertt',['sensorFunctionConvertT',['../sensor_8c.html#afe3ae54abaa137530ed4bd0f7ab7fa67',1,'sensorFunctionConvertT():&#160;sensor.c'],['../sensor_8h.html#a618c67f65901568362b27fdbb29fde20',1,'sensorFunctionConvertT(void):&#160;sensor.c']]],
  ['sensorfunctionreadscratchpad',['sensorFunctionReadScratchpad',['../sensor_8c.html#a59bb3a70828e3daa51e7e84426d7c16d',1,'sensorFunctionReadScratchpad(void):&#160;sensor.c'],['../sensor_8h.html#a59bb3a70828e3daa51e7e84426d7c16d',1,'sensorFunctionReadScratchpad(void):&#160;sensor.c']]],
  ['sensorfunctionwritescratchpad',['sensorFunctionWriteScratchpad',['../sensor_8c.html#a9874f8896c4bceb7b2a798ef3996f643',1,'sensorFunctionWriteScratchpad(uint8_t TH, uint8_t TL, uint8_t config):&#160;sensor.c'],['../sensor_8h.html#a9874f8896c4bceb7b2a798ef3996f643',1,'sensorFunctionWriteScratchpad(uint8_t TH, uint8_t TL, uint8_t config):&#160;sensor.c']]],
  ['sensorreset',['sensorReset',['../sensor_8c.html#a2bf5c755fbf2345a7df80121b0de3d6f',1,'sensorReset(void):&#160;sensor.c'],['../sensor_8h.html#a2bf5c755fbf2345a7df80121b0de3d6f',1,'sensorReset(void):&#160;sensor.c']]],
  ['sensorromcommandskip',['sensorROMCommandSkip',['../sensor_8c.html#a07d7e82dd26016ecb686fc85398e0d56',1,'sensorROMCommandSkip(void):&#160;sensor.c'],['../sensor_8h.html#a07d7e82dd26016ecb686fc85398e0d56',1,'sensorROMCommandSkip(void):&#160;sensor.c']]],
  ['setd4',['setD4',['../io_8c.html#a5ced1cc3849505f28ac7bf9b68e926ce',1,'setD4(bool state):&#160;io.c'],['../io_8h.html#a5ced1cc3849505f28ac7bf9b68e926ce',1,'setD4(bool state):&#160;io.c']]],
  ['setd5',['setD5',['../io_8c.html#a875f8bbabe1a3d13ed00431795ae3f21',1,'setD5(bool state):&#160;io.c'],['../io_8h.html#a875f8bbabe1a3d13ed00431795ae3f21',1,'setD5(bool state):&#160;io.c']]],
  ['setd6',['setD6',['../io_8c.html#a2ecf9dbc5d7f0fe559a08fedcf4ce728',1,'setD6(bool state):&#160;io.c'],['../io_8h.html#a2ecf9dbc5d7f0fe559a08fedcf4ce728',1,'setD6(bool state):&#160;io.c']]],
  ['setd7',['setD7',['../io_8c.html#af74fb38d31bcdf7ddcf7a42130fc806c',1,'setD7(bool state):&#160;io.c'],['../io_8h.html#af74fb38d31bcdf7ddcf7a42130fc806c',1,'setD7(bool state):&#160;io.c']]]
];
